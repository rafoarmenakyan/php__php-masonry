<?php
    require_once 'connect.php';


    $validationTypeImg = false;
    $validationFields = false;
    if (isset($_POST["add"])) {

        $title = $_POST['title'];
        $color = $_POST['color'];
        $fileName = $_FILES['file']['name'];
        
        if (!(empty($title)) && !(empty($color)) && !(empty($fileName))) {
            $allow = ["jpg", "jpeg", "gif", "png"];
            $uniqueImgId = false;
            if (in_array( pathinfo($fileName, PATHINFO_EXTENSION), $allow)) {
                $imgFolder = 'img/';
                $uniqueImgId = uniqid() . '_' . basename($fileName);
                move_uploaded_file( $_FILES['file']['tmp_name'], $imgFolder . $uniqueImgId);
                $createdAt = date('Y-m-d H:i:s');
                mysqli_query($conn,"INSERT INTO $fileTable (path,title,color,createdAt) VALUES ('$uniqueImgId','$title','$color','$createdAt')");
            } else {
                $validationTypeImg = true;        
            }
        } else {
            $validationFields = true;
        } 
    } else if (isset($_POST["save"])) {

        $editedTitle = $_POST['editTitle'];
        $editedColor = $_POST['editColor'];
        $id = $_POST['id'];
        mysqli_query($conn, "UPDATE $fileTable SET title='$editedTitle', color='$editedColor' WHERE id=$id");

    } else if (isset($_POST['delete'])) {
        
        $id = $_POST['id'];
        $result = $conn->query("SELECT path FROM $fileTable WHERE id=$id");
        $path = $result->fetch_assoc()['path'];
        mysqli_query($conn,"DELETE FROM $fileTable WHERE id=$id");
        if(!empty($path) && file_exists('img/'.$path)) {
            @unlink('img/'.$path);
        }
    }

    $result = $conn->query("SELECT * FROM $fileTable");
    $items = [];
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $items[] = $row;
        }
    }

    $conn->close();
?>




<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">


</head>
<body>



    <?php if($validationTypeImg) { ?>
        <p class="validationTxt">This File Is Not a Image Type !!!</p>
    <?php } ?>
    

    <?php if($validationFields) { ?>
        <p class="validationTxt">Declare All Fields !!!</p>
    <?php } ?>

    
    <?php foreach ($items as $item) { ?>
        <div class="getBlock">
            <form action="admin.php" class="getForm" method="post">
                <input type="hidden" name="id" value="<?php echo $item['id'] ?>">
                <img src="img/<?php echo $item["path"] ?>" alt="" class="getImg">
                <input type="text" value="<?php echo $item["title"] ?>" class="getTitleInp" name="editTitle">
                <input type="text" value="<?php echo $item["color"] ?>" class="getColorInp" name="editColor">
                <input type="submit" value="save" class="getAddInp" name="save">
                <input type="submit" value="delete" class="getSubmitInp" name="delete">
            </form>
        </div>
    <?php } ?>
            
     

    <div class="addFileBlock">
        <form action="admin.php" method="post" enctype="multipart/form-data">
            <input type="file" name="file"  class="addFileInp">
            <input type="text" name="title" placeholder="Title" class="addTitleInp">
            <input type="text" name="color" placeholder="Color" class="addColorInp">
            <input type="submit" value="add" name="add" class="addSubmitInp">
        </form>
    </div>

    
</body>
</html>