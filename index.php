<?php
    require_once 'connect.php';

    $sql = "SELECT * FROM $fileTable";
    $result = $conn->query($sql);

    $items = [];
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $items[] = $row;
        }
    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        .masonry {
            position: relative;
        }

        .masonry__item {
            list-style-type: none;
            position: absolute;
        }

        .masonry__image{
            width: 100%;
            height: 100%;
            display: block;
        }
    </style>
</head>
<body>
    


    <?php foreach ($items as $item) { ?>
        <ul class="masonry">
            <li class="masonry__item"><img src="img/<?php echo $item["path"] ?>" style='border:2px solid <?php echo  $item["color"]?>'  class="masonry__image"></li>
        </ul>
    <?php } ?>

    
       

<script>
    function Masonry (DOMclass, sizing) {
        this.DOMclass = DOMclass,
        this.columnWidth = sizing.columnWidth
        this.autoResize = sizing.autoResize
    }

    Masonry.prototype.render = function(){
        const elements = Array.from(document.querySelectorAll(`${this.DOMclass} li`))
        let columnWidth = this.columnWidth
        let [elementsHeight,elementsWidthSum,elementMinHeight,minHeightIndex,elementFromLeft] = [[],0,0,0,0]
        elements.forEach((element,index) => {
            element.style.cssText = `width:${columnWidth}px`
            elementsWidthSum += element.offsetWidth
            if(elementsWidthSum <= window.innerWidth){
                elementsHeight.push(element.offsetHeight)
                element.style.cssText = `left:${elementsWidthSum-columnWidth}px; width:${columnWidth}px`
                return
            }
            elementMinHeight = Math.min(...elementsHeight)
            minHeightIndex = elementsHeight.indexOf(elementMinHeight)
            elementFromLeft = elements[minHeightIndex].style.left
            element.style.cssText = `top:${elementMinHeight}px; left:${elementFromLeft}; width:${columnWidth}px`
            elementsHeight[minHeightIndex] = elementsHeight[minHeightIndex] + element.offsetHeight
        })

    }
    window.addEventListener('resize', function(){
        let autoResize = masonry.autoResize;
        if(autoResize){
            masonry.render()
            return
        }
    })



    const masonry = new Masonry('.masonry', {
        columnWidth: 300,
        autoResize: true
    })

    masonry.render()

</script>

</body>
</html>