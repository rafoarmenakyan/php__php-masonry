-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 23, 2021 at 10:44 AM
-- Server version: 10.3.22-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `filedescription`
--

CREATE TABLE `filedescription` (
  `id` int(11) NOT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `title` varchar(32) CHARACTER SET utf8mb4 NOT NULL,
  `color` varchar(32) CHARACTER SET utf8mb4 NOT NULL,
  `createdAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `filedescription`
--

INSERT INTO `filedescription` (`id`, `path`, `title`, `color`, `createdAt`) VALUES
(228, '6082786da0327_00-best-backgrounds-and-wallpaper-apps-for-android.jpg', '1', 'red', '2021-04-23 10:34:05'),
(229, '608278744a204_1ab4136c2ab60215be9a25ba0d2dc534.jpg', '2', 'blue', '2021-04-23 10:34:12'),
(231, '608278842dc08_2079033abc8314be554f9d24f562a199.jpg', '4', 'green', '2021-04-23 10:34:28'),
(232, '6082788c49042_20516-afterlives-ardenweald-4k-desktop-wallpapers.jpg', '5', 'yellow', '2021-04-23 10:34:36'),
(234, '6082789a618ea_a4f8f91b31d2c63a015ed34ae8c13bbd.jpg', '7', 'aqua', '2021-04-23 10:34:50'),
(235, '608278a0bc909_20516-afterlives-ardenweald-4k-desktop-wallpapers.jpg', '8', 'orange', '2021-04-23 10:34:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `filedescription`
--
ALTER TABLE `filedescription`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `filedescription`
--
ALTER TABLE `filedescription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
